<?xml version="1.0" ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>EVP_KDF-SCRYPT</title>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rev="made" href="mailto:root@localhost" />
</head>

<body style="background-color: white">


<!-- INDEX BEGIN -->
<div name="index">
<p><a name="__index__"></a></p>

<ul>

	<li><a href="#name">NAME</a></li>
	<li><a href="#description">DESCRIPTION</a></li>
	<ul>

		<li><a href="#identity">Identity</a></li>
		<li><a href="#supported_parameters">Supported parameters</a></li>
	</ul>

	<li><a href="#notes">NOTES</a></li>
	<li><a href="#examples">EXAMPLES</a></li>
	<li><a href="#conforming_to">CONFORMING TO</a></li>
	<li><a href="#see_also">SEE ALSO</a></li>
	<li><a href="#copyright">COPYRIGHT</a></li>
</ul>

<hr name="index" />
</div>
<!-- INDEX END -->

<p>
</p>
<hr />
<h1><a name="name">NAME</a></h1>
<p>EVP_KDF-SCRYPT - The scrypt EVP_KDF implementation</p>
<p>
</p>
<hr />
<h1><a name="description">DESCRIPTION</a></h1>
<p>Support for computing the <strong>scrypt</strong> password-based KDF through the <strong>EVP_KDF</strong>
API.</p>
<p>The EVP_KDF-SCRYPT algorithm implements the scrypt password-based key
derivation function, as described in <a href="http://www.ietf.org/rfc/rfc7914.txt" class="rfc">RFC 7914</a>.  It is memory-hard in the sense
that it deliberately requires a significant amount of RAM for efficient
computation. The intention of this is to render brute forcing of passwords on
systems that lack large amounts of main memory (such as GPUs or ASICs)
computationally infeasible.</p>
<p>scrypt provides three work factors that can be customized: N, r and p. N, which
has to be a positive power of two, is the general work factor and scales CPU
time in an approximately linear fashion. r is the block size of the internally
used hash function and p is the parallelization factor. Both r and p need to be
greater than zero. The amount of RAM that scrypt requires for its computation
is roughly (128 * N * r * p) bytes.</p>
<p>In the original paper of Colin Percival (&quot;Stronger Key Derivation via
Sequential Memory-Hard Functions&quot;, 2009), the suggested values that give a
computation time of less than 5 seconds on a 2.5 GHz Intel Core 2 Duo are N =
2^20 = 1048576, r = 8, p = 1. Consequently, the required amount of memory for
this computation is roughly 1 GiB. On a more recent CPU (Intel i7-5930K at 3.5
GHz), this computation takes about 3 seconds. When N, r or p are not specified,
they default to 1048576, 8, and 1, respectively. The maximum amount of RAM that
may be used by scrypt defaults to 1025 MiB.</p>
<p>
</p>
<h2><a name="identity">Identity</a></h2>
<p>&quot;SCRYPT&quot; is the name for this implementation; it
can be used with the <code>EVP_KDF_fetch()</code> function.</p>
<p>
</p>
<h2><a name="supported_parameters">Supported parameters</a></h2>
<p>The supported parameters are:</p>
<dl>
<dt><strong><a name="pass_ossl_kdf_param_password_octet_string" class="item">&quot;pass&quot; (<strong>OSSL_KDF_PARAM_PASSWORD</strong>) &lt;octet string&gt;</a></strong></dt>

<dt><strong><a name="salt_ossl_kdf_param_salt_octet_string" class="item">&quot;salt&quot; (<strong>OSSL_KDF_PARAM_SALT</strong>) &lt;octet string&gt;</a></strong></dt>

<dd>
<p>These parameters work as described in <em>EVP_KDF(3)/PARAMETERS</em>.</p>
</dd>
<dt><strong><a name="n_ossl_kdf_param_scrypt_n_unsigned_integer" class="item">&quot;n&quot; (<strong>OSSL_KDF_PARAM_SCRYPT_N</strong>) &lt;unsigned integer&gt;</a></strong></dt>

<dt><strong><a name="r_ossl_kdf_param_scrypt_r_unsigned_integer" class="item">&quot;r&quot; (<strong>OSSL_KDF_PARAM_SCRYPT_R</strong>) &lt;unsigned integer&gt;</a></strong></dt>

<dt><strong><a name="p_ossl_kdf_param_scrypt_p_unsigned_integer" class="item">&quot;p&quot; (<strong>OSSL_KDF_PARAM_SCRYPT_P</strong>) &lt;unsigned integer&gt;</a></strong></dt>

<dd>
<p>These parameters configure the scrypt work factors N, r and p.
N is a parameter of type <strong>uint64_t</strong>.
Both r and p are parameters of type <strong>uint32_t</strong>.</p>
</dd>
</dl>
<p>
</p>
<hr />
<h1><a name="notes">NOTES</a></h1>
<p>A context for scrypt can be obtained by calling:</p>
<pre>
 EVP_KDF *kdf = EVP_KDF_fetch(NULL, &quot;SCRYPT&quot;, NULL);
 EVP_KDF_CTX *kctx = EVP_KDF_CTX_new(kdf);</pre>
<p>The output length of an scrypt key derivation is specified via the
&quot;keylen&quot; parameter to the <em>EVP_KDF_derive(3)</em> function.</p>
<p>
</p>
<hr />
<h1><a name="examples">EXAMPLES</a></h1>
<p>This example derives a 64-byte long test vector using scrypt with the password
&quot;password&quot;, salt &quot;NaCl&quot; and N = 1024, r = 8, p = 16.</p>
<pre>
 EVP_KDF *kdf;
 EVP_KDF_CTX *kctx;
 unsigned char out[64];
 OSSL_PARAM params[6], *p = params;</pre>
<pre>
 kdf = EVP_KDF_fetch(NULL, &quot;SCRYPT&quot;, NULL);
 kctx = EVP_KDF_CTX_new(kdf);
 EVP_KDF_free(kdf);</pre>
<pre>
 *p++ = OSSL_PARAM_construct_octet_string(OSSL_KDF_PARAM_PASSWORD,
                                          &quot;password&quot;, (size_t)8);
 *p++ = OSSL_PARAM_construct_octet_string(OSSL_KDF_PARAM_SALT,
                                          &quot;NaCl&quot;, (size_t)4);
 *p++ = OSSL_PARAM_construct_uint64(OSSL_KDF_PARAM_SCRYPT_N, (uint64_t)1024);
 *p++ = OSSL_PARAM_construct_uint32(OSSL_KDF_PARAM_SCRYPT_R, (uint32_t)8);
 *p++ = OSSL_PARAM_construct_uint32(OSSL_KDF_PARAM_SCRYPT_P, (uint32_t)16);
 *p = OSSL_PARAM_construct_end();
 if (EVP_KDF_CTX_set_params(kctx, params) &lt;= 0) {
     error(&quot;EVP_KDF_CTX_set_params&quot;);
 }
 if (EVP_KDF_derive(kctx, out, sizeof(out)) &lt;= 0) {
     error(&quot;EVP_KDF_derive&quot;);
 }</pre>
<pre>
 {
     const unsigned char expected[sizeof(out)] = {
         0xfd, 0xba, 0xbe, 0x1c, 0x9d, 0x34, 0x72, 0x00,
         0x78, 0x56, 0xe7, 0x19, 0x0d, 0x01, 0xe9, 0xfe,
         0x7c, 0x6a, 0xd7, 0xcb, 0xc8, 0x23, 0x78, 0x30,
         0xe7, 0x73, 0x76, 0x63, 0x4b, 0x37, 0x31, 0x62,
         0x2e, 0xaf, 0x30, 0xd9, 0x2e, 0x22, 0xa3, 0x88,
         0x6f, 0xf1, 0x09, 0x27, 0x9d, 0x98, 0x30, 0xda,
         0xc7, 0x27, 0xaf, 0xb9, 0x4a, 0x83, 0xee, 0x6d,
         0x83, 0x60, 0xcb, 0xdf, 0xa2, 0xcc, 0x06, 0x40
     };</pre>
<pre>
     assert(!memcmp(out, expected, sizeof(out)));
 }</pre>
<pre>
 EVP_KDF_CTX_free(kctx);</pre>
<p>
</p>
<hr />
<h1><a name="conforming_to">CONFORMING TO</a></h1>
<p>RFC 7914</p>
<p>
</p>
<hr />
<h1><a name="see_also">SEE ALSO</a></h1>
<p><em>EVP_KDF(3)</em>,
<em>EVP_KDF_CTX_new(3)</em>,
<em>EVP_KDF_CTX_free(3)</em>,
<em>EVP_KDF_CTX_set_params(3)</em>,
<em>EVP_KDF_derive(3)</em>,
<em>EVP_KDF(3)/PARAMETERS</em></p>
<p>
</p>
<hr />
<h1><a name="copyright">COPYRIGHT</a></h1>
<p>Copyright 2017-2019 The OpenSSL Project Authors. All Rights Reserved.</p>
<p>Licensed under the Apache License 2.0 (the &quot;License&quot;).  You may not use
this file except in compliance with the License.  You can obtain a copy
in the file LICENSE in the source distribution or at
<a href="https://www.openssl.org/source/license.html">https://www.openssl.org/source/license.html</a>.</p>

</body>

</html>
